FROM python:3.8
WORKDIR /code
COPY . .
RUN pip install -r requirements.txt
EXPOSE 8000

ENTRYPOINT [ "/bin/bash", "-c", "uwsgi --http 0.0.0.0:8000 --wsgi-file app.py --master --processes 1 --threads 1" ]
